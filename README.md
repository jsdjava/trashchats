# Traschats
## Demo

[![Thumbnail](pics/trashchats.png)](https://trashchats.com?apikey=garden-scandal-hammer-bargain "Trashchats - Click to open website")
[Click image (or here) to open website](https://trashchats.com?apikey=garden-scandal-hammer-bargain)

## Description
Trashchats is a small webapp I created to demo expiring chat rooms that die after 15 minutes.
I also implemented an "apikey" access key check to keep the demo not easily accessible on the internet, and
added an age check for entering a chat that 404s if you are too young (<18)

Major features completed are:
* Soft "api key" access check for gating the demo
* Mobile/desktop view
* 404, Legal, Privacy, Contact pages
* Main page
* Room creation + entry page
* Chat room page
* Expired chat room
* Terraformed in AWS (route53, cloudfront, appsync, s3, dynamodb)
* Chat built out using pub-sub graphql through appsync
* Vtl resolvers that perform read/write/updates in dynamo
* React frontend + some pure html pages

Other features I may implement in the future are:
* Potentially releasing it to the wider internet

## Running Locally
Not really possible because it uses appsync and VTL, but you can build and run the frontend using `npm run start`

## Attribution
Icon was sourced from https://freeiconshop.com/icon/trash-icon-flat/

Legal/Privacy forms were built using WebsitePolicies.com

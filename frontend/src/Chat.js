import React,{useState, useEffect} from 'react';
import {graphql} from 'react-apollo';
import HashLoader from 'react-spinners/HashLoader';
import queryString from 'query-string';

import Heading from './Heading.js';
import MessageInput from './MessageInput.js';
import MessagesList from './MessagesList.js';
import SignIn from './SignIn.js';
import Home from './Home.js';

import CreateChat from './mutations/createChat.js';

import "./Chat.css";

const getSignIn = ()=> JSON.parse(window.localStorage.getItem("trashchat-sign-in") || "{}");

const storeSignIn = (signIn)=>{
  window.localStorage.setItem("trashchat-sign-in",JSON.stringify(signIn));
}

function Chat({createChat}){
  const {chatName} = queryString.parse(window.location.search);
  const [chat,setChat] = useState();
  const [signIn,setSignIn] = useState(getSignIn());

  useEffect(()=>{
    if(!chatName) return;
    createChat({
      variables:{
        chatName,
      }
    }).then(x=>{
      setChat(x.data.createChat);
    });
  },[]);
  return chatName ? (chat && signIn.allowedToChat!==false) ? <div className="chat">
    <Heading chatName={chatName} createdAt={chat.createdAt} onExpiration={()=>setChat({expired:true})}/>
    {
      chat.expired ? <h3 className="expiredChat"> Chat has been trashed </h3> :
      signIn.name ? <div className="chatContainerDiv">
        <MessagesList chatName={chatName} initialMessages = {chat.messages.messages} initialNextToken = {chat.messages.nextToken}/>
        <MessageInput chatName={chatName} author={signIn.name}/>
      </div> : <SignIn onSignIn={signIn=>{
        storeSignIn(signIn)
        setSignIn(signIn);
      }}/>
    }
  </div> : <div className="chatStateDiv"><HashLoader size = {50} color="purple" loading={true}/></div>:<Home/>;
}

export default graphql(CreateChat,{
  name: 'createChat',
})(Chat);

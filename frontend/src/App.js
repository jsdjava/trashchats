import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";

import './App.css';
import Chat from './Chat';
import Footer from './Footer';
import Legal from './Legal.js';
import Contact from './Contact.js';
import Privacy from "./Privacy.js";

function App(){
  return <Router>
    <div className="App">
      <Switch>
        <Route path="/legal">
          <Legal/>
        </Route>
        <Route path="/privacy">
          <Privacy/>
        </Route>
        <Route path="/contact">
          <Contact/>
        </Route>
        <Route path="/" render={(props) => <Chat key={props.location.key} />}/>
      </Switch>
      <Footer/>
    </div>
  </Router>;
}

export default App;

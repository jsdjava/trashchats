import React,{useState} from 'react';
import {graphql} from 'react-apollo';
import {TextField} from '@material-ui/core';

import CreateMessage from './mutations/createMessage.js';

import "./MessageInput.css";

function SendMessage({createMessage,chatName,author}){
  const [message,setMessage] = useState("");
  let sendMessage;
  return <div className="messageInputDiv">
    <TextField
      multiline={true} 
      rows={3}
      variant="outlined"
      value = {message}
      onKeyPress = {e=>{
        if(e.key === 'Enter' && !e.shiftKey){
            sendMessage = true;
          }
        }}
      onChange = {e=>{
        if(sendMessage && e.target.value.trim()){
          createMessage({
            variables:{
              chatName,
              content:e.target.value.trim(),
              author:author,
            }
          });
          setMessage("");
        } else{
          setMessage(e.target.value.split("\n").slice(0,3).join("\n").slice(0,400));
        }
        sendMessage = false;
      }}/>
  </div>
}

export default graphql(CreateMessage,{
  name: 'createMessage',
})(SendMessage);
import {Button} from '@material-ui/core';
import randomWords from 'random-words';
import queryString from 'query-string';

import "./Home.css";

function Home(){
  return <div className="homeDiv">
      <div className="topRow">
        <h1>Welcome to Trash Chat</h1>
        <ol>
          <li> Create a chat room </li>
          <li> Share the link </li>
          <li> Start chatting </li>
          <li> Chat expires after 15 minutes </li>        
        </ol>
      </div>
      <div className="bottomRow">
        <Button onClick={()=>{
          const parsedQueryString = queryString.parse(window.location.search);
          parsedQueryString.chatName = randomWords(3).join("-"); 
          window.location.href = `/?${queryString.stringify(parsedQueryString)}`
        }}>Start chatting!
        </Button>
      </div>
  </div>
}

export default Home;
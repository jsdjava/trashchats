import React,{useState, useEffect} from 'react';
import {graphql} from 'react-apollo';
import ScrollToBottom from 'react-scroll-to-bottom';
import HashLoader from "react-spinners/HashLoader";

import Message from "./Message.js";
import "./MessagesList.css";

import GetMessages from './queries/getMessages.js';
import OnCreateMessage from './subscriptions/onCreateMessage.js';

function MessagesList({initialMessages,chatName,getMessages,initialNextToken}){
  const [messages,setMessages] = useState(initialMessages);
  const [nextToken,setNextToken] = useState();
  useEffect(()=>{
    if(getMessages.loading || getMessages.error){
      return;
    }

    // by default, both the createChat query and the getMessages query will fire,
    // but if there aren't enough messages the getMessages query returns the same data.
    if(initialNextToken){
      setMessages(x=>[...x,...getMessages.allMessageConnection.messages]);
      setNextToken(getMessages.allMessageConnection.nextToken);
    }
    getMessages.subscribeToMore({
      document:OnCreateMessage,
      variables:{
        chatName,
      },
      updateQuery:(p,{subscriptionData})=>{
        setMessages(x=>{
          return [subscriptionData.data.onCreateMessage,...x]
        });
      },
    });
  },[getMessages.loading,getMessages.error]);
  if(getMessages.loading){
    return <div className="messagesListStateDiv"><HashLoader size = {50} color = "purple" loading={true}/></div>;
  }
  if(getMessages.error){
    return <div className="messagesListStateDiv"><h3>Error!</h3></div>; 
  }
  return <ScrollToBottom className="messagesList">
    <div className="loadMoreMessagesDiv">
    {nextToken ? <a href="" onClick={(e)=>{
      e.preventDefault();
      getMessages.fetchMore({
        variables:{
          after:nextToken,
        },
        updateQuery:(p,{fetchMoreResult})=>{
          setMessages(x=>[...x,...fetchMoreResult.allMessageConnection.messages])
          setNextToken(fetchMoreResult.allMessageConnection.nextToken);
        },
      });
    }}>Load more</a> : null}
    </div>
    {[...messages].reverse().map(x=><Message {...x}/>)}
 </ScrollToBottom>;
}

export default graphql(GetMessages,{
  name: 'getMessages',
  options: props=>({variables:{
    chatName:props.chatName,
    after:props.initialNextToken,
  }})
})(MessagesList);

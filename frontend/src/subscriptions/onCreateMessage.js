import gql from 'graphql-tag';

export default gql`
  subscription OnCreateMessage($chatName: String!) {
    onCreateMessage(chatName: $chatName) {
      author
      content
      chatName
      createdAt
      id
    }
  } 
`
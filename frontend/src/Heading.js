import React,{useState} from 'react';
import LinkIcon from '@material-ui/icons/Link';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import moment from 'moment';
import Countdown from 'react-countdown';

import {ReactComponent as Trash} from './trash.svg';

import "./Heading.css";

// TODO The 15 minute limit is double coded...

function Heading({chatName,createdAt,onExpiration}){
  const [copied,setCopied] = useState();
  return <div className="heading">
    <Trash/>
    <h2>{chatName}</h2>
    {createdAt ? <div className="rightDiv">
      <Countdown date={moment(createdAt).add(15,'minutes').toDate()} onComplete={onExpiration}/>
      {!copied ? <CopyToClipboard text={window.location.href} onCopy={()=>{
        setCopied(true);
        setTimeout(()=>setCopied(false),2000);
      }}>
        <LinkIcon/>
      </CopyToClipboard> : <p>Copied!</p>}
    </div> : null }
  </div>;
}

export default Heading;

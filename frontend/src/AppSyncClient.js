import AWSAppSyncClient from 'aws-appsync'
import { ApolloProvider } from 'react-apollo'
import { Rehydrated } from 'aws-appsync-react'

import App from './App.js';

const config = {
  region: 'us-east-1',
  graphqlEndpoint: 'https://bsx2kuosjnh2vfbbcohduxbhuq.appsync-api.us-east-1.amazonaws.com/graphql',
  apiKey: "da2-ltbr6fknkvgnjearkzloqdejly",
}

const client = new AWSAppSyncClient({
  url: config.graphqlEndpoint,
  region: config.region,
  auth: {
    type: "API_KEY",
    apiKey:config.apiKey,
  },
  disableOffline: true
},{
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore'
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'all'
    }
  }
})

const WithProvider = () => (
  <ApolloProvider client={client}>
    <Rehydrated>
        <App/>
    </Rehydrated>
  </ApolloProvider>
)

export default WithProvider;
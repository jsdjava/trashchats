import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import WithProvider from './AppSyncClient';

ReactDOM.render(
  <WithProvider />,
  document.getElementById('root')
);

import gql from 'graphql-tag';

export default gql`mutation CreateMessage($author:String!,$content: String!, $chatName: String!){
  createMessage(author:$author,content:$content,chatName:$chatName){
    author
    content
    chatName
    createdAt
    id
  }
}`;
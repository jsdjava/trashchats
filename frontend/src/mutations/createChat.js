import gql from 'graphql-tag';

export default gql`mutation CreateChat($chatName: String!){
  createChat(name: $chatName){
    createdAt
    name
    expired
    messages(first: 10){
      messages{
        author
        content
        chatName
        createdAt
        id
      }
      nextToken
    }
  }
}`;
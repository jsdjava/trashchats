import React,{useState} from "react";

import {TextField, Button} from '@material-ui/core';

import "./Contact.css";

const formSpreeUrl = "https://formspree.io/f/mpzolwdk";

const maxNameLength = 40;
const maxEmailLength = 40;
const maxMessageLength = 400;

function Contact(){
  const [name,setName] = useState("");
  const [email,setEmail] = useState("");
  const [message,setMessage] = useState("");
  return <div className="contactDiv">
    <h2>Contact</h2>
    <TextField 
      label="Name"
      multiline={true}
      value={name}
      rows={1}
      variant="outlined"
      maxLength={maxNameLength}
      InputLabelProps={{
        shrink:false,
        classes:{
          formControl:"nickNameLabelFormControl",
          outlined:"nickNameLabelOutlined",
        },
      }}
      onKeyPress = {e=>{
        if(e.key === 'Enter'){
          e.preventDefault();
        }
      }}
      onChange={(e)=>{
        const updatedName = e.target.value.replace(/[^A-Za-z\d]/gi,"")
        setName(updatedName.slice(0,maxNameLength));
      }}/>
    <TextField 
      label="Email"
      multiline={true}
      value={email} 
      rows={1}
      variant="outlined"
      maxLength={maxEmailLength}
      InputLabelProps={{
        shrink:false,
        classes:{
          formControl:"nickNameLabelFormControl",
          outlined:"nickNameLabelOutlined",
        },
      }}
      onKeyPress = {e=>{
        if(e.key === 'Enter'){
          e.preventDefault();
        }
      }}
      onChange={(e)=>{
        const updatedEmail = e.target.value.replace(/[^A-Za.-z@\d]/gi,"")
        setEmail(updatedEmail.slice(0,maxEmailLength));
      }}/>
    <TextField 
      label="Message"
      multiline={true} 
      value={message}
      rows={10}
      variant="outlined"
      maxLength={maxMessageLength}
      InputLabelProps={{
        shrink:false,
        classes:{
          formControl:"nickNameLabelFormControl",
          outlined:"nickNameLabelOutlined",
        },
      }}
      onKeyPress = {e=>{
        if(e.key === 'Enter'){
          e.preventDefault();
        }
      }}
      onChange={(e)=>{
        const updatedMessage = e.target.value.replace(/[^A-Za-z\d]/gi,"")
        setMessage(updatedMessage.slice(0,maxMessageLength));
      }}/>
    <Button disabled = {!(message && name && email)} onClick={()=>{
      const formData = new FormData();
      formData.append('name',name);
      formData.append('email',email);
      formData.append('message',message);
      fetch(formSpreeUrl,{
        mode:'no-cors',
        method:'POST',
        body:formData
      });
      window.location.href = "/";
    }}>Send</Button>
  </div>;
}

export default Contact;
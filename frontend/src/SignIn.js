import React,{useState} from 'react';
import 'date-fns';
import differenceInYears from 'date-fns/differenceInYears'
import {TextField ,Button} from '@material-ui/core';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

import "./SignIn.css";

const maxLength = 40;

function SignIn({onSignIn}){
  const [name,setName] = useState(); 
  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  return <div className="setNameDiv">
    <TextField
      label="Nickname"
      value={name}
      multiline={true} 
      rows={1}
      variant="outlined"
      maxLength={maxLength}
      InputLabelProps={{
        shrink:false,
        classes:{
          formControl:"nickNameLabelFormControl",
          outlined:"nickNameLabelOutlined",
        },
      }}
      onKeyPress = {e=>{
        if(e.key === 'Enter'){
          e.preventDefault();
        }
      }}
      onChange={(e)=>{
        const updatedName = e.target.value.replace(/[^A-Za-z\d]/gi,"")
        setName(updatedName.slice(0,maxLength));
      }}/>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        disableToolbar
        variant="inline"
        format="MM/dd/yyyy"
        margin="normal"
        id="date-picker-inline"
        label="Birthday"
        value={selectedDate}
        onChange={handleDateChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
      />
    </MuiPickersUtilsProvider>
    <Button disabled = {!(name && selectedDate)} onClick={()=>{
      onSignIn({
        allowedToChat:differenceInYears(new Date(),selectedDate)>=18,
        name,
      });
    }}>Start Chatting</Button>
  </div>
}

export default SignIn;
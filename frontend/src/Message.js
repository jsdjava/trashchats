import moment from 'moment';
import { ReactTinyLink } from 'react-tiny-link'
import {
  GiMiteAlt,
  GiSpiderFace,
  GiEarthWorm,
  GiBoarEnsign,
  GiRam,
  GiRabbit,
  GiBlackCat,
  GiJumpingDog,
  GiElephant,
  GiSnake,
  GiKiwiBird,
  GiFly,
  GiBee,
  GiKoala,
  GiAnglerFish,
  GiKangaroo,
  GiPanda,
  GiTiger,
  GiPolarBear,
  GiFlyingFox,
  GiSpermWhale,
} from "react-icons/gi";

import "./Message.css";

const avatars = [
  GiMiteAlt,
  GiSpiderFace,
  GiEarthWorm,
  GiBoarEnsign,
  GiRam,
  GiRabbit,
  GiBlackCat,
  GiJumpingDog,
  GiElephant,
  GiSnake,
  GiKiwiBird,
  GiFly,
  GiBee,
  GiKangaroo,
  GiAnglerFish,
  GiKoala,
  GiPanda,
  GiTiger,
  GiPolarBear,
  GiFlyingFox,
  GiSpermWhale,
]

// https://coolors.co/d00000-ffba08-3f88c5-032b43-136f63
const colors = [
  "#D00000",
  "#FFBA08",
  "#3F88C5",
  "#032B43",
  "#136F63",
];

const getAuthorAvatar = (author)=>{
  const authorHash = author.split('').reduce((acc,x)=>acc+x.charCodeAt(),0);
  return avatars[authorHash % avatars.length];
}

const getAuthorColor = (author)=>{
  const authorHash = author.split('').reduce((acc,x)=>acc+x.charCodeAt(),0);
  return colors[authorHash % colors.length];
}

function Message({author,content,createdAt}){
  const [link] = (content.match(/https:\/\/(.*)(?:\s|$)/) || [])
  const Avatar = getAuthorAvatar(author);
  return <div className="messageDiv">
    <div className="messageIcon">
      <Avatar size="35px" color = {getAuthorColor(author)}/>
    </div>
    <div className="messageRightPanel">
      <div className="messageAuthor">
        {author}
      </div>
      <div className="messageCreatedAt">
        {moment(createdAt).format('MMMM Do YYYY, h:mm:ss a')}
      </div>
      <div className="messageContent">
        {content.split("\n").map(x=><span>{x}<br/></span>)}
        {link ?<ReactTinyLink
          cardSize="small"
          showGraphic={true}
          maxLine={2}
          minLine={1}
          url={link}
        /> : null}
      </div>
    </div>
  </div>
};

export default Message;
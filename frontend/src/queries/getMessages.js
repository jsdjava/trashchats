import gql from 'graphql-tag';

export default gql`query getMessages($chatName: String!, $after: String){
  allMessageConnection(chatName: $chatName, after: $after){
    messages{
    author
    content
    chatName
    createdAt
    id
    }
    nextToken
  }
}`;
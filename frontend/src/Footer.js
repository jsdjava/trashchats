import React from 'react';
import queryString from 'query-string';
import {
  Link
} from "react-router-dom";

import "./Footer.css";

const stickyApiKeyLink = (pathname)=>()=>{
  const {apikey} = queryString.parse(window.location.search);
  return {
    pathname,
    search: queryString.stringify({apikey}),
  }
}

function Footer(){
  return <footer>
    <Link to={stickyApiKeyLink("/")}>Home</Link>
    <Link to={stickyApiKeyLink("/legal")}>Legal</Link>
    <Link to={stickyApiKeyLink("/privacy")}>Privacy</Link>
    <Link to={stickyApiKeyLink("/contact")}>Contact</Link>
  </footer>;
}

export default Footer;

resource "aws_s3_bucket" "trashchats" {
  bucket = "trashchats.com"
  
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "aws_s3_bucket_policy" "trashchats-s3-policy" {
  bucket = "trashchats.com"
  policy = data.aws_iam_policy_document.trashchats-cloudfront-policy.json
}

data "aws_iam_policy_document" "trashchats-cloudfront-policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.trashchats.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.trashchats.iam_arn]
    }
  }
}

resource "aws_cloudfront_origin_access_identity" "trashchats" {}

resource "aws_cloudfront_distribution" "trashchats" {
  origin {
    domain_name = aws_s3_bucket.trashchats.bucket_regional_domain_name
    origin_id   = "s3"
    s3_origin_config {
      origin_access_identity =  aws_cloudfront_origin_access_identity.trashchats.cloudfront_access_identity_path
    }
  }
  
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = ["www.trashchats.com","trashchats.com"]

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "s3"

    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = aws_lambda_function.check_apikey.qualified_arn
      include_body = false
    }

    forwarded_values {
      query_string = true
      cookies {
        forward = "whitelist"
        whitelisted_names=["apikey"]
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 300
    default_ttl            = 300
    max_ttl                = 300
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.trashchats.arn
    ssl_support_method = "sni-only"
  }

  price_class = "PriceClass_All"

  custom_error_response {
    error_code = 404
    response_code=404
    response_page_path = "/404.html"
  }
}

data "archive_file" "check_apikey_zip" {
  type        = "zip"
  output_path = "${path.module}/tmp/check_apikey_zip.zip"
  source_dir = "${path.module}/../check-apikey"
}

resource "aws_lambda_function" "check_apikey" {
  filename         = "${data.archive_file.check_apikey_zip.output_path}"
  source_code_hash = "${data.archive_file.check_apikey_zip.output_base64sha256}"
  function_name = "trashchats-check-apikey"
  role          = aws_iam_role.edge_role.arn
  handler       = "checkApiKey.handler"
  runtime       = "nodejs12.x"
  publish = true
}

resource "aws_iam_role" "edge_role" {
  name = "trashchats-edge"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

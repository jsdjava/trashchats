resource "aws_route53_zone" "trashchats" {
  name = "trashchats.com"
}

resource "aws_route53_record" "trashchats-validate" {
  zone_id         = aws_route53_zone.trashchats.zone_id
  name = "${aws_acm_certificate.trashchats.domain_validation_options.0.resource_record_name}"
  type = "${aws_acm_certificate.trashchats.domain_validation_options.0.resource_record_type}"
  records = ["${aws_acm_certificate.trashchats.domain_validation_options.0.resource_record_value}"]
  ttl = "300"
}

resource "aws_route53_record" "www-trashchats-validate" {
  zone_id         = aws_route53_zone.trashchats.zone_id
  name = "${aws_acm_certificate.trashchats.domain_validation_options.1.resource_record_name}"
  type = "${aws_acm_certificate.trashchats.domain_validation_options.1.resource_record_type}"
  records = ["${aws_acm_certificate.trashchats.domain_validation_options.1.resource_record_value}"]
  ttl = "300"
}

resource "aws_route53_record" "trashchats-apex-cloudfront" {
  zone_id         = aws_route53_zone.trashchats.zone_id
  name    = "trashchats.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.trashchats.domain_name
    zone_id                = aws_cloudfront_distribution.trashchats.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "trashchats-www-cloudfront" {
  zone_id         = aws_route53_zone.trashchats.zone_id
  name    = "www.trashchats.com"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.trashchats.domain_name
    zone_id                = aws_cloudfront_distribution.trashchats.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "trashchats" {
  allow_overwrite = true
  name            = "trashchats.com"
  ttl             = 30
  type            = "NS"
  zone_id         = aws_route53_zone.trashchats.zone_id

  records = [
    aws_route53_zone.trashchats.name_servers[0],
    aws_route53_zone.trashchats.name_servers[1],
    aws_route53_zone.trashchats.name_servers[2],
    aws_route53_zone.trashchats.name_servers[3],
  ]
}

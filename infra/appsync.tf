resource "aws_appsync_graphql_api" "trash_chat" {
  authentication_type = "API_KEY"
  name                = "TrashChat"
  schema = file("../backend/schema/schema.gql")
}

resource "aws_appsync_api_key" "trash_chat" {
  api_id  = aws_appsync_graphql_api.trash_chat.id
  expires = "2022-11-07T00:00:00Z"
}

resource "aws_iam_role" "trash_chat_appsync" {
  name = "TrashChatAppsync"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "appsync.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_appsync_datasource" "trash_chat_chats" {
  api_id           = aws_appsync_graphql_api.trash_chat.id
  name             = "TrashChatChats"
  service_role_arn = aws_iam_role.trash_chat_appsync.arn
  type             = "AMAZON_DYNAMODB"

  dynamodb_config {
    table_name = aws_dynamodb_table.trash_chat_chats.name
  }
}

resource "aws_appsync_datasource" "trash_chat_messages" {
  api_id           = aws_appsync_graphql_api.trash_chat.id
  name             = "TrashChatMessages"
  service_role_arn = aws_iam_role.trash_chat_appsync.arn
  type             = "AMAZON_DYNAMODB"

  dynamodb_config {
    table_name = aws_dynamodb_table.trash_chat_messages.name
  }
}

resource "aws_appsync_function" "create_chat" {
  api_id                   = aws_appsync_graphql_api.trash_chat.id
  data_source              = aws_appsync_datasource.trash_chat_chats.name
  name                     = "CreateChat"
  request_mapping_template = file("../backend/functions/CreateChat.request.vtl")
  response_mapping_template = "$util.toJson($ctx.result)"
}

resource "aws_appsync_function" "get_chat" {
  api_id                   = aws_appsync_graphql_api.trash_chat.id
  data_source              = aws_appsync_datasource.trash_chat_chats.name
  name                     = "GetChat"
  request_mapping_template = file("../backend/functions/GetChat.request.vtl")
  response_mapping_template = "$util.toJson($ctx.result)"
}

resource "aws_appsync_resolver" "mutation_create_chat" {
  type              = "Mutation"
  api_id            = aws_appsync_graphql_api.trash_chat.id
  field             = "createChat"
  request_template  = "{}"
  response_template = "$util.toJson($ctx.result)"
  kind              = "PIPELINE"
  pipeline_config {
    functions = [
      aws_appsync_function.get_chat.function_id,
      aws_appsync_function.create_chat.function_id,
    ]
  }
}

resource "aws_appsync_resolver" "mutation_create_message" {
  api_id      = aws_appsync_graphql_api.trash_chat.id
  field       = "createMessage"
  type        = "Mutation"
  data_source = aws_appsync_datasource.trash_chat_messages.name
  request_template = file("../backend/resolvers/Mutation.createMessage.request.vtl")
  response_template = "$util.toJson($ctx.result)"
}

resource "aws_appsync_resolver" "query_all_message_connection" {
  api_id      = aws_appsync_graphql_api.trash_chat.id
  field       = "allMessageConnection"
  type        = "Query"
  data_source = aws_appsync_datasource.trash_chat_messages.name
  request_template = file("../backend/resolvers/Query.allMessageConnection.request.vtl")
  response_template = file("../backend/resolvers/Query.allMessageConnection.response.vtl")
}

resource "aws_appsync_resolver" "query_messages" {
  api_id      = aws_appsync_graphql_api.trash_chat.id
  field = "messages"
  type = "Chat"
  data_source = aws_appsync_datasource.trash_chat_messages.name
  request_template = file("../backend/resolvers/Query.messages.request.vtl")
  response_template = file("../backend/resolvers/Query.messages.response.vtl")
}
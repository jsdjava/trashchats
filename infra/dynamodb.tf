resource "aws_dynamodb_table" "trash_chat_chats" {
  name           = "TrashChatChats"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "name"

  attribute {
    name = "name"
    type = "S"
  }
}

resource "aws_dynamodb_table" "trash_chat_messages" {
  name           = "TrashChatMessages"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "chatName"
  range_key      = "createdAt"

  attribute {
    name = "chatName"
    type = "S"
  }

  attribute {
    name = "createdAt"
    type = "S"
  }
}

resource "aws_iam_role_policy" "trash_chat_grant_appsync_dynamodb_access" {
  name = "TrashChatGrantAppSyncDynamoDbAccess"
  role = aws_iam_role.trash_chat_appsync.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_dynamodb_table.trash_chat_chats.arn}",
        "${aws_dynamodb_table.trash_chat_messages.arn}"
      ]
    }
  ]
}
EOF
}
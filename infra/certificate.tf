resource "aws_acm_certificate" "trashchats" {
  domain_name = "trashchats.com"
  subject_alternative_names = ["www.trashchats.com"]
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" trashchats {
  certificate_arn = "${aws_acm_certificate.trashchats.arn}"
  validation_record_fqdns = [
    "${aws_route53_record.trashchats-validate.fqdn}",
    "${aws_route53_record.www-trashchats-validate.fqdn}",
  ]
  timeouts{
    create="60m"
  }
}

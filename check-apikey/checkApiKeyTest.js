const {handler} = require('./checkApiKey.js');

const awsEvent = {
  Records:[{
    cf:{
      request:{
        headers:{
          cookie:[{value:"apikey=garden-scandal-hammer-bargain"}]
        },
        querystring:"?apikey=garden-scandal-hammer-bargain"
      }
    }
  }]
}

handler(awsEvent,null,(a,resp)=>{
  console.log(resp);
})
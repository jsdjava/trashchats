const cookie = require('cookie');
const queryString = require('query-string');
const expectedApiKey = "garden-scandal-hammer-bargain";

exports.handler = async (event, context, callback) => {
  const {request} = event.Records[0].cf;
  const queryParams = queryString.parse(request.querystring);

  if(request.headers.cookie){
    const cookies = cookie.parse(request.headers.cookie[0].value);
    if(cookies.apikey===expectedApiKey){
      return callback(null, request);
    }
  }
  if(queryParams.apikey === expectedApiKey){
    return callback(null,{
      status:302,
      headers:{
        'set-cookie':[{
          key:'Set-Cookie',
          value:`apikey=${expectedApiKey}; domain=trashchats.com`
        }],
        'location':[{
          key:'Location',
          value: "https://www.trashchats.com",
        }],
        'cache-control':[{
          key:'Cache-Control',
          value: 'no-cache, no-store, must-revalidate',
        }],
        'pragma':[{
          key:'pragma',
          value: 'no-cache',
        }],
        'expires':[{
          key:'Expires',
          value: '0',
        }]
      }
    });
  }
  request.uri = "/404.html";
  return callback(null, request);
};
